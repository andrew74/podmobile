package com.example.abm.podorojnik;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

public class Main4ActivitySchedulePodorojnik extends AppCompatActivity {

    // Установка размера шрифта TextView.
    int tvTextSize = 20;
    // Установка цвета текста TextView по умолчанию.
    // 1-я строка таблица время, 1-я строка таблица бокс, время, бокс, фокус время.
    int[] tvTextColor = new  int[] {0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff};
    // Установка цвета фона TextView  по умолчанию.
    // 1-я строка таблица время, 1-я строка таблица бокс, время, бокс, фокус время.
    int[] tvBackgroundColor = new int[] {0xff008000, 0xff008000, 0xffffffff, 0xffcccccc, 0xff008000};
    // Дата. Получуние текущей даты.
    Calendar DateSchedulePodorojnik = Calendar.getInstance();
    // Для получении даты.
    Button bnDateSchedulePodorojnik;
    // Для таймера и обработчика событий.
    MyTimerTask mTimerTask;
    Timer timer;
    // Описание TextView для мойки.
    TextView tvAddressSchedulePodorojnik;
    // Описание TextView для бокса.
    TextView tvBoxPodorojnik;
    // Описание TextView для времени.
    TextView tvTime0000Podorojnik;
    TextView tvTime0015Podorojnik;
    TextView tvTime0030Podorojnik;
    TextView tvTime0045Podorojnik;
    TextView tvTime0100Podorojnik;
    TextView tvTime0115Podorojnik;
    TextView tvTime0130Podorojnik;
    TextView tvTime0145Podorojnik;
    TextView tvTime0200Podorojnik;
    TextView tvTime0215Podorojnik;
    TextView tvTime0230Podorojnik;
    // Описание TextView для бокса.
    TextView tvTime0000BoxPodorojnik;
    TextView tvTime0015BoxPodorojnik;
    TextView tvTime0030BoxPodorojnik;
    TextView tvTime0045BoxPodorojnik;
    TextView tvTime0100BoxPodorojnik;
    TextView tvTime0115BoxPodorojnik;
    TextView tvTime0130BoxPodorojnik;
    TextView tvTime0145BoxPodorojnik;
    TextView tvTime0200BoxPodorojnik;
    TextView tvTime0215BoxPodorojnik;
    TextView tvTime0230BoxPodorojnik;

   // Класс для получения адресов.
    // Инициализируем массив адресов и боксов.
   List<Wash> washes;
    List<Box> boxesWash_0;
    List<Box> boxesWash_1;
    List<Box> boxesWash_2;
    List<Box> boxesWash_3;
    List<Box> boxesWash_4;

    class NetworkAsyncHelper extends AsyncTask<Void, Void, Integer> {
        @Override
        protected Integer doInBackground(Void... params) {

            // Мойки.
            washes = PodkomiHelper.getWashes();

            return 0;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Устанавливаем заголовок.
        setTitle("РАСПИСАНИЕ");

        // Создаём макет ScrollView.

        ScrollView scrollViewSchedulePodorojnik = new ScrollView(this);

        // Установка цвета фона макета.
        scrollViewSchedulePodorojnik.setBackgroundColor(0xff000000);

        // Создаём макет LinerLayout.
        LinearLayout linearLayoutSchedulePodorojnik = new LinearLayout(this);

        // Вертикальная ореинтация.
        linearLayoutSchedulePodorojnik.setOrientation(LinearLayout.VERTICAL);

        // Установка цвета фона макета.
        linearLayoutSchedulePodorojnik.setBackgroundColor(0xff000000);

        // Создаём новый TextView.
        tvAddressSchedulePodorojnik = new TextView(this);

        // Текст на TextView.
        tvAddressSchedulePodorojnik.setText("Выберите мойку");

        // Установка размера шрифта TextView.
        tvAddressSchedulePodorojnik.setTextSize(20);

        // Установка цвета текста TextView.
        tvAddressSchedulePodorojnik.setTextColor(0xffffffff);

        // Настраиваем вид TextView на макете.
        LinearLayout.LayoutParams layoutParamsAddressSchedulePodorojnik = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // Установка внешних отступов TextView на макете.
        layoutParamsAddressSchedulePodorojnik.setMargins(20, 20, 20, 20);

        // Установка внешних отступов TextView на макете.
        tvAddressSchedulePodorojnik.setLayoutParams(layoutParamsAddressSchedulePodorojnik);

        // Добавляем TextView на макет.
        linearLayoutSchedulePodorojnik.addView(tvAddressSchedulePodorojnik);

        // Создаём кнопку.
        bnDateSchedulePodorojnik = new Button(this);

        // Установка цвета текста Button.
        bnDateSchedulePodorojnik.setTextColor(0xffffffff);

        // Установка размера шрифта Button.
        bnDateSchedulePodorojnik.setTextSize(20);

        // устанавливаем вравнивание текста Button.
        bnDateSchedulePodorojnik.setTextAlignment(Button.TEXT_ALIGNMENT_TEXT_START);

        // Настраиваем вид кнопки на макете.
        LinearLayout.LayoutParams layoutParamsDateSchedulePodorojnikABM = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // Установка внешних отступов кнопки на макете.
        layoutParamsDateSchedulePodorojnikABM.setMargins(20, 20, 20, 20);

        // Установка внешних отступов на кнопке.
        bnDateSchedulePodorojnik.setLayoutParams(layoutParamsDateSchedulePodorojnikABM);

        // Добавляем кнопку на макет.
        linearLayoutSchedulePodorojnik.addView(bnDateSchedulePodorojnik);

        // Устанавливваем начальную дату.
        setInitialDate();

        // Обработка события нажатия кнопки.
        bnDateSchedulePodorojnik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // отображаем диалоговое окно для выбора даты
                setDateSchedulePodorojnik();
            }
        });

        // Добавление LinearLayout в ScrollView.
        scrollViewSchedulePodorojnik.addView(linearLayoutSchedulePodorojnik);

        // Создаём макет TableLayout.
        TableLayout tableLayout = new TableLayout(this);

        // Вертикальная ореинтация.
        tableLayout.setOrientation(TableLayout.VERTICAL);

        // Добавление TableLayout в LinearLayout.
        linearLayoutSchedulePodorojnik.addView(tableLayout);

        // для растягивания всех столбцов
        tableLayout.setStretchAllColumns(true);

        // Создаём новый TextView время, бокс.
        TextView tvTimeLeftPodorojnik = new TextView(this);
        tvBoxPodorojnik = new TextView(this);
        // Создаём новый TextView для времени.
        tvTime0000Podorojnik = new TextView(this);
        tvTime0015Podorojnik = new TextView(this);
        tvTime0030Podorojnik = new TextView(this);
        tvTime0045Podorojnik = new TextView(this);
        tvTime0100Podorojnik = new TextView(this);
        tvTime0115Podorojnik = new TextView(this);
        tvTime0130Podorojnik = new TextView(this);
        tvTime0145Podorojnik = new TextView(this);
        tvTime0200Podorojnik = new TextView(this);
        tvTime0215Podorojnik = new TextView(this);
        tvTime0230Podorojnik = new TextView(this);
        // Создаём новый TextView для бокса.
        tvTime0000BoxPodorojnik = new TextView(this);
        tvTime0015BoxPodorojnik = new TextView(this);
        tvTime0030BoxPodorojnik = new TextView(this);
        tvTime0045BoxPodorojnik = new TextView(this);
        tvTime0100BoxPodorojnik = new TextView(this);
        tvTime0115BoxPodorojnik = new TextView(this);
        tvTime0130BoxPodorojnik = new TextView(this);
        tvTime0145BoxPodorojnik = new TextView(this);
        tvTime0200BoxPodorojnik = new TextView(this);
        tvTime0215BoxPodorojnik = new TextView(this);
        tvTime0230BoxPodorojnik = new TextView(this);

        // Установка текста на TextView время, бокс.
        tvTimeLeftPodorojnik.setText("Время");
        tvBoxPodorojnik.setText("Выберите бокс");
        // Установка текста на TextView  для времени.
        tvTime0000Podorojnik.setText("00:00");
        tvTime0015Podorojnik.setText("00:15");
        tvTime0030Podorojnik.setText("00:30");
        tvTime0045Podorojnik.setText("00:45");
        tvTime0100Podorojnik.setText("01:00");
        tvTime0115Podorojnik.setText("01:15");
        tvTime0130Podorojnik.setText("01:30");
        tvTime0145Podorojnik.setText("01:45");
        tvTime0200Podorojnik.setText("02:00");
        tvTime0215Podorojnik.setText("02:15");
        tvTime0230Podorojnik.setText("02:30");
        // Установка текста на TextView для бокса.
        tvTime0000BoxPodorojnik.setText("Шевроле НИВА 4x4");
        tvTime0015BoxPodorojnik.setText("Комплексная");
        tvTime0030BoxPodorojnik.setText("Воск");
        tvTime0045BoxPodorojnik.setText("Силикон");
        tvTime0100BoxPodorojnik.setText("Двигатель");
        tvTime0115BoxPodorojnik.setText("650 р.");
        tvTime0130BoxPodorojnik.setText("72 мин.");
        tvTime0145BoxPodorojnik.setText("Рено Дастер");
        tvTime0200BoxPodorojnik.setText("Комплексная");
        tvTime0215BoxPodorojnik.setText("13 р. 13 копейек.");
        tvTime0230BoxPodorojnik.setText("13 минут 13 секунд 13 милисекунд.");

        // Устанавливаем вравнивание текста по центру TextView время, бокс.
        tvTimeLeftPodorojnik.setTextAlignment(TextView.TEXT_ALIGNMENT_CENTER);
        tvBoxPodorojnik.setTextAlignment(TextView.TEXT_ALIGNMENT_CENTER);

        // Установка размера шрифта TextView время, бокс.
        tvTimeLeftPodorojnik.setTextSize(tvTextSize);
        tvBoxPodorojnik.setTextSize(tvTextSize);
        // Установка размера шрифта TextView для времени.
        tvTime0000Podorojnik.setTextSize(tvTextSize);
        tvTime0015Podorojnik.setTextSize(tvTextSize);
        tvTime0030Podorojnik.setTextSize(tvTextSize);
        tvTime0045Podorojnik.setTextSize(tvTextSize);
        tvTime0100Podorojnik.setTextSize(tvTextSize);
        tvTime0115Podorojnik.setTextSize(tvTextSize);
        tvTime0130Podorojnik.setTextSize(tvTextSize);
        tvTime0145Podorojnik.setTextSize(tvTextSize);
        tvTime0200Podorojnik.setTextSize(tvTextSize);
        tvTime0215Podorojnik.setTextSize(tvTextSize);
        tvTime0230Podorojnik.setTextSize(tvTextSize);
        // Установка размера шрифта TextView для бокса.
        tvTime0000BoxPodorojnik.setTextSize(tvTextSize);
        tvTime0015BoxPodorojnik.setTextSize(tvTextSize);
        tvTime0030BoxPodorojnik.setTextSize(tvTextSize);
        tvTime0045BoxPodorojnik.setTextSize(tvTextSize);
        tvTime0100BoxPodorojnik.setTextSize(tvTextSize);
        tvTime0115BoxPodorojnik.setTextSize(tvTextSize);
        tvTime0130BoxPodorojnik.setTextSize(tvTextSize);
        tvTime0145BoxPodorojnik.setTextSize(tvTextSize);
        tvTime0200BoxPodorojnik.setTextSize(tvTextSize);
        tvTime0215BoxPodorojnik.setTextSize(tvTextSize);
        tvTime0230BoxPodorojnik.setTextSize(tvTextSize);

        // Установка цвета текста TextView время, бокс.
        tvTimeLeftPodorojnik.setTextColor(tvTextColor[0]);
        tvBoxPodorojnik.setTextColor(tvTextColor[1]);
        // Установка цвета текста TextView для времени.
        tvTime0000Podorojnik.setTextColor(tvTextColor[2]);
        tvTime0015Podorojnik.setTextColor(tvTextColor[2]);
        tvTime0030Podorojnik.setTextColor(tvTextColor[2]);
        tvTime0045Podorojnik.setTextColor(tvTextColor[2]);
        tvTime0100Podorojnik.setTextColor(tvTextColor[2]);
        tvTime0115Podorojnik.setTextColor(tvTextColor[2]);
        tvTime0130Podorojnik.setTextColor(tvTextColor[2]);
        tvTime0145Podorojnik.setTextColor(tvTextColor[2]);
        tvTime0200Podorojnik.setTextColor(tvTextColor[2]);
        tvTime0215Podorojnik.setTextColor(tvTextColor[2]);
        tvTime0230Podorojnik.setTextColor(tvTextColor[2]);
        // Установка цвета текста TextView для бокса.
        tvTime0000BoxPodorojnik.setTextColor(tvTextColor[3]);
        tvTime0015BoxPodorojnik.setTextColor(tvTextColor[3]);
        tvTime0030BoxPodorojnik.setTextColor(tvTextColor[3]);
        tvTime0045BoxPodorojnik.setTextColor(tvTextColor[3]);
        tvTime0100BoxPodorojnik.setTextColor(tvTextColor[3]);
        tvTime0115BoxPodorojnik.setTextColor(tvTextColor[3]);
        tvTime0130BoxPodorojnik.setTextColor(tvTextColor[3]);
        tvTime0145BoxPodorojnik.setTextColor(tvTextColor[3]);
        tvTime0200BoxPodorojnik.setTextColor(tvTextColor[3]);
        tvTime0215BoxPodorojnik.setTextColor(tvTextColor[3]);
        tvTime0230BoxPodorojnik.setTextColor(tvTextColor[3]);

        // Установка цвета фона TextView время, бокс.
        tvTimeLeftPodorojnik.setBackgroundColor(tvBackgroundColor[0]);
        tvBoxPodorojnik.setBackgroundColor(tvBackgroundColor[1]);
        // Установка цвета фона TextView для времени.
        tvTime0000Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
        tvTime0015Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
        tvTime0030Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
        tvTime0045Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
        tvTime0100Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
        tvTime0115Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
        tvTime0130Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
        tvTime0145Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
        tvTime0200Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
        tvTime0215Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
        tvTime0230Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
        // Установка цвета фона TextView для бокса.
        tvTime0000BoxPodorojnik.setBackgroundColor(tvBackgroundColor[3]);
        tvTime0015BoxPodorojnik.setBackgroundColor(tvBackgroundColor[3]);
        tvTime0030BoxPodorojnik.setBackgroundColor(tvBackgroundColor[3]);
        tvTime0045BoxPodorojnik.setBackgroundColor(tvBackgroundColor[3]);
        tvTime0100BoxPodorojnik.setBackgroundColor(tvBackgroundColor[3]);
        tvTime0115BoxPodorojnik.setBackgroundColor(tvBackgroundColor[3]);
        tvTime0130BoxPodorojnik.setBackgroundColor(tvBackgroundColor[3]);
        tvTime0145BoxPodorojnik.setBackgroundColor(tvBackgroundColor[3]);
        tvTime0200BoxPodorojnik.setBackgroundColor(tvBackgroundColor[3]);
        tvTime0215BoxPodorojnik.setBackgroundColor(tvBackgroundColor[3]);
        tvTime0230BoxPodorojnik.setBackgroundColor(tvBackgroundColor[3]);

        // Установка фокуса TextView для времени.
        tvTime0000Podorojnik.setFocusable(true);
        tvTime0015Podorojnik.setFocusable(true);
        tvTime0030Podorojnik.setFocusable(true);
        tvTime0045Podorojnik.setFocusable(true);
        tvTime0100Podorojnik.setFocusable(true);
        tvTime0115Podorojnik.setFocusable(true);
        tvTime0130Podorojnik.setFocusable(true);
        tvTime0145Podorojnik.setFocusable(true);
        tvTime0200Podorojnik.setFocusable(true);
        tvTime0215Podorojnik.setFocusable(true);
        tvTime0230Podorojnik.setFocusable(true);

        // Установка фокуса TextView для сенсорного экрана для времени.
        tvTime0000Podorojnik.setFocusableInTouchMode(true);
        tvTime0015Podorojnik.setFocusableInTouchMode(true);
        tvTime0030Podorojnik.setFocusableInTouchMode(true);
        tvTime0045Podorojnik.setFocusableInTouchMode(true);
        tvTime0100Podorojnik.setFocusableInTouchMode(true);
        tvTime0115Podorojnik.setFocusableInTouchMode(true);
        tvTime0130Podorojnik.setFocusableInTouchMode(true);
        tvTime0145Podorojnik.setFocusableInTouchMode(true);
        tvTime0200Podorojnik.setFocusableInTouchMode(true);
        tvTime0215Podorojnik.setFocusableInTouchMode(true);
        tvTime0230Podorojnik.setFocusableInTouchMode(true);

        // Создаём новые строки для TableLayout.
        TableRow tableRow1 = new TableRow(this);
        TableRow tableRow2 = new TableRow(this);
        TableRow tableRow3 = new TableRow(this);
        TableRow tableRow4 = new TableRow(this);
        TableRow tableRow5 = new TableRow(this);
        TableRow tableRow6 = new TableRow(this);
        TableRow tableRow7 = new TableRow(this);
        TableRow tableRow8 = new TableRow(this);
        TableRow tableRow9 = new TableRow(this);
        TableRow tableRow10 = new TableRow(this);
        TableRow tableRow11 = new TableRow(this);
        TableRow tableRow12 = new TableRow(this);

        // Добавляем TextView в строку TableLayout время, бокс.
        tableRow1.addView(tvTimeLeftPodorojnik);
        tableRow1.addView(tvBoxPodorojnik);
        // Добавляем TextView в строку TableLayout для времени.
        tableRow2.addView(tvTime0000Podorojnik);
        tableRow3.addView(tvTime0015Podorojnik);
        tableRow4.addView(tvTime0030Podorojnik);
        tableRow5.addView(tvTime0045Podorojnik);
        tableRow6.addView(tvTime0100Podorojnik);
        tableRow7.addView(tvTime0115Podorojnik);
        tableRow8.addView(tvTime0130Podorojnik);
        tableRow9.addView(tvTime0145Podorojnik);
        tableRow10.addView(tvTime0200Podorojnik);
        tableRow11.addView(tvTime0215Podorojnik);
        tableRow12.addView(tvTime0230Podorojnik);
        // Добавляем TextView в строку TableLayout  для бокса.
        tableRow2.addView(tvTime0000BoxPodorojnik);
        tableRow3.addView(tvTime0015BoxPodorojnik);
        tableRow4.addView(tvTime0030BoxPodorojnik);
        tableRow5.addView(tvTime0045BoxPodorojnik);
        tableRow6.addView(tvTime0100BoxPodorojnik);
        tableRow7.addView(tvTime0115BoxPodorojnik);
        tableRow8.addView(tvTime0130BoxPodorojnik);
        tableRow9.addView(tvTime0145BoxPodorojnik);
        tableRow10.addView(tvTime0200BoxPodorojnik);
        tableRow11.addView(tvTime0215BoxPodorojnik);
        tableRow12.addView(tvTime0230BoxPodorojnik);

        // Добавляем строку в TableLayout.
        tableLayout.addView(tableRow1);
        tableLayout.addView(tableRow2);
        tableLayout.addView(tableRow3);
        tableLayout.addView(tableRow4);
        tableLayout.addView(tableRow5);
        tableLayout.addView(tableRow6);
        tableLayout.addView(tableRow7);
        tableLayout.addView(tableRow8);
        tableLayout.addView(tableRow9);
        tableLayout.addView(tableRow10);
        tableLayout.addView(tableRow11);
        tableLayout.addView(tableRow12);

        // Отображаем макет.
        setContentView(scrollViewSchedulePodorojnik);

        // Таймер.
        if(timer != null){
            timer.cancel();
            timer = null;
        }
        timer = new Timer();

        // выполняем задачу MyTimerTask, описание которой будет ниже
        mTimerTask = new MyTimerTask();
        timer.schedule(mTimerTask, 1000, 5000);

    }
    // Метод для описания Menu.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Создание меню.
        super.onCreateOptionsMenu(menu);

        // Вызов класса для получения адресов.
        NetworkAsyncHelper netHelper = new NetworkAsyncHelper();
        netHelper.execute();

        // Ожидание загрузки данных.
        // Метод get() блокирует основной UI-поток и ждёт завершения AsyncTask.
        if (netHelper == null)
        {
            return false;
        }
        int result =-1;
        try {
            result = netHelper.get();
        } catch (InterruptedException e) {
            // Обработчик ошибки.
            e.printStackTrace();
        } catch (ExecutionException e) {
            // Обработчик ошибки.
            e.printStackTrace();
        }

        // Создание меню.
        if( 1 <= washes.size() ) {

            SubMenu subMenuWash_0 = menu.addSubMenu(washes.get(0).getName());
            // Боксы.
            boxesWash_0 = washes.get(0).getBoxes();
            for( int CountBox = 0; CountBox < boxesWash_0.size(); CountBox++) {
                subMenuWash_0.add(Menu.NONE, 101 + CountBox, Menu.NONE, boxesWash_0.get(CountBox).getDescription());
            }
        }
        if( 2 <= washes.size() ) {

            SubMenu subMenuWash_1 = menu.addSubMenu(washes.get(1).getName());
            // Боксы.
            boxesWash_1 = washes.get(1).getBoxes();
            for ( int CountBox = 0; CountBox < boxesWash_1.size(); CountBox++) {
                subMenuWash_1.add(Menu.NONE, 201 + CountBox, Menu.NONE, boxesWash_1.get(CountBox).getDescription());
            }
        }
        if ( 3 <= washes.size() ) {

            SubMenu subMenuWash_2 = menu.addSubMenu(washes.get(2).getName());
            // Боксы.
            boxesWash_2 = washes.get(2).getBoxes();
            for ( int CountBox  = 0; CountBox < boxesWash_2.size(); CountBox++) {
                subMenuWash_2.add(Menu.NONE, 301 + CountBox, Menu.NONE, boxesWash_2.get(CountBox).getDescription());
            }
        }
        if ( 4 <= washes.size() ) {

            SubMenu subMenuWash_3 = menu.addSubMenu(washes.get(3).getName());
            // Боксы.
            boxesWash_3 = washes.get(3).getBoxes();
            for (int CountBox = 0; CountBox < boxesWash_3.size(); CountBox++ ) {
                subMenuWash_3.add(Menu.NONE, 401 + CountBox, Menu.NONE, boxesWash_3.get(CountBox).getDescription());
            }
        }
        if ( 5 <= washes.size() ) {

            SubMenu subMenuWash_4 = menu.addSubMenu(washes.get(4).getName());
            // Боксы.
            boxesWash_4 = washes.get(4).getBoxes();
            for ( int CountBox = 0; CountBox < boxesWash_4.size(); CountBox++) {
                subMenuWash_4.add(Menu.NONE, 501 + CountBox, Menu.NONE, boxesWash_4.get(CountBox).getDescription());
            }
        }
        if ( 6 <= washes.size() ) {

            SubMenu subMenuWash_5 = menu.addSubMenu(washes.get(5).getName());
            // Боксы.
            subMenuWash_5.add(Menu.NONE, 601, Menu.NONE, "Тестовый бокс");
        }
        if ( 7 <= washes.size() ) {

        SubMenu subMenuWash_6 = menu.addSubMenu(washes.get(6).getName());
        // Боксы.
        }

        //  Группа. id. порядок. заголовок.
        menu.add(0, 1, 0, "НАЗАД");
        menu.add(0,2,2, "ЗАПИСАТЬСЯ НА МОЙКУ");
        menu.add(0, 3, 3, "СПРАВКА");


        return true;
    }
    @Override
    public boolean onOptionsItemSelected (MenuItem item) {

        //
        int id = item.getItemId();

        // abm
        Toast toastMenu;

        //
        switch (id) {
            case 1 :

                // Пункт Menu "НАЗАД".
                // Всплывающее окно
                toastMenu = Toast.makeText(getApplicationContext(), item.getTitle().toString(), Toast.LENGTH_LONG);
                toastMenu.show();
                return true;
            case 2:

                // Пункт Menu "ЗАПИСАТЬСЯ НА МОЙКУ".
                // действия, совершаемые после нажатия на кнопку
                // Создаем объект Intent для вызова новой Activity
                // в первом параметре текущий класс. во втором - класс для перехода
                Intent intentMenuRecordSinkPodorojnik = new Intent(this, Main2ActivityRecordSinkPodorojnik.class);

                // запуск activity
                startActivity(intentMenuRecordSinkPodorojnik);

                // Всплывающее окно
                toastMenu = Toast.makeText(getApplicationContext(), item.getTitle().toString(), Toast.LENGTH_LONG);
                toastMenu.show();
                return true;
            case 3:

                // Пункт Menu  "СПРАВКА".
                // Всплывающее окно
                toastMenu = Toast.makeText(getApplicationContext(), item.getTitle().toString(), Toast.LENGTH_LONG);
                toastMenu.show();

                return true;
            case 101:
                // Мойка.
                tvAddressSchedulePodorojnik.setText(washes.get(0).getName());
                // Бокс.
                tvBoxPodorojnik.setText(boxesWash_0.get(0).getDescription());

                return true;
            case 102:
                // Мойка.
                tvAddressSchedulePodorojnik.setText(washes.get(0).getName());
                // Бокс.
                tvBoxPodorojnik.setText(boxesWash_0.get(1).getDescription());

                return true;
        }

        //
        return super.onOptionsItemSelected(item);
    }
    // Конец метода для описания Menu.
    // Начало метода для описания Calendare.
    // установка обработчика выбора даты
    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {


            DateSchedulePodorojnik.set(Calendar.YEAR, i);
            DateSchedulePodorojnik.set(Calendar.MONTH, i1);
            DateSchedulePodorojnik.set(Calendar.DAY_OF_MONTH, i2);
            setInitialDate();
        }
    };
    // отображаем диалоговое окно для выбора даты
    public void setDateSchedulePodorojnik () {

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, d, DateSchedulePodorojnik.get(Calendar.YEAR), DateSchedulePodorojnik.get(Calendar.MONTH), DateSchedulePodorojnik.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }
    // установка начальных даты и времени
    private void setInitialDate () {

        bnDateSchedulePodorojnik.setText(DateUtils.formatDateTime(this, DateSchedulePodorojnik.getTimeInMillis(), DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_SHOW_WEEKDAY));
    }
    // Конец метода для описания Calendare.
    // Метод для описания того, что будет происходить при работе таймера (задача для таймера):
    class  MyTimerTask extends TimerTask {
        @Override
        public void run () {

            //  Способ запуска Runnable в UI-потоке.
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    // Удаление TextView для времени.
                    /* tvTime0000Podorojnik.setVisibility(View.GONE);
                    tvTime0015Podorojnik.setVisibility(View.GONE);
                    tvTime0030Podorojnik.setVisibility(View.GONE);
                    tvTime0045Podorojnik.setVisibility(View.GONE);
                    tvTime0100Podorojnik.setVisibility(View.GONE);
                    tvTime0115Podorojnik.setVisibility(View.GONE);
                    tvTime0130Podorojnik.setVisibility(View.GONE);
                    tvTime0145Podorojnik.setVisibility(View.GONE);
                    tvTime0200Podorojnik.setVisibility(View.GONE);
                    tvTime0215Podorojnik.setVisibility(View.GONE);
                    tvTime0230Podorojnik.setVisibility(View.GONE); */
                    // Удаление TextView для бокса.
                    /* tvTime0000BoxPodorojnik.setVisibility(View.GONE);
                    tvTime0015BoxPodorojnik.setVisibility(View.GONE);
                    tvTime0030BoxPodorojnik.setVisibility(View.GONE);
                    tvTime0045BoxPodorojnik.setVisibility(View.GONE);
                    tvTime0100BoxPodorojnik.setVisibility(View.GONE);
                    tvTime0115BoxPodorojnik.setVisibility(View.GONE);
                    tvTime0130BoxPodorojnik.setVisibility(View.GONE);
                    tvTime0145BoxPodorojnik.setVisibility(View.GONE);
                    tvTime0200BoxPodorojnik.setVisibility(View.GONE);
                    tvTime0215BoxPodorojnik.setVisibility(View.GONE);
                    tvTime0230BoxPodorojnik.setVisibility(View.GONE); */

                    // Берем дату и время с системного календаря
                    Calendar calendar = Calendar.getInstance();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss a");

                    // Получение системной минуты, часа, секунды.
                    int minuteSystem = calendar.get(Calendar.MINUTE);
                    //24 hour format
                    int hourDaySystem = calendar.get(Calendar.HOUR_OF_DAY);
                    int secondSystem = calendar.get(Calendar.SECOND);

                    // Время с начала суток в секундах.
                    int SecondDay = hourDaySystem * 3600 + minuteSystem * 60 + secondSystem;

                    // abm test.
                    int abmSecondDay = 14 * 3600 + 30 * 60;
                    SecondDay = SecondDay - abmSecondDay;

                    // Интервал времени работы маркера 900 секунд.
                    int IntervalTime = 15;

                    if (SecondDay >= 0 & SecondDay <= IntervalTime){
                        // Фокус TextView время.
                        tvTime0000Podorojnik.requestFocus();
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0000Podorojnik.setBackgroundColor(tvBackgroundColor[4]);
                        tvTime0000Podorojnik.setTextColor(tvTextColor[4]);
                    } else {
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0000Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
                        tvTime0000Podorojnik.setTextColor(tvTextColor[2]);
                    }
                    if (SecondDay >= (IntervalTime + 1) & SecondDay <= (IntervalTime*2)){
                        // Фокус TextView время.
                        tvTime0015Podorojnik.requestFocus();
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0015Podorojnik.setBackgroundColor(tvBackgroundColor[4]);
                        tvTime0015Podorojnik.setTextColor(tvTextColor[4]);
                    } else {
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0015Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
                        tvTime0015Podorojnik.setTextColor(tvTextColor[2]);
                    }
                    if (SecondDay >= (IntervalTime*2+1) & SecondDay <= (IntervalTime*3)){
                        // Фокус TextView время.
                        tvTime0030Podorojnik.requestFocus();
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0030Podorojnik.setBackgroundColor(tvBackgroundColor[4]);
                        tvTime0030Podorojnik.setTextColor(tvTextColor[4]);
                    } else {
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0030Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
                        tvTime0030Podorojnik.setTextColor(tvTextColor[2]);
                    }
                    if (SecondDay >= (IntervalTime*3+1) & SecondDay <= (IntervalTime*4)){
                        // Фокус TextView время.
                        tvTime0045Podorojnik.requestFocus();
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0045Podorojnik.setBackgroundColor(tvBackgroundColor[4]);
                        tvTime0045Podorojnik.setTextColor(tvTextColor[4]);
                    } else {
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0045Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
                        tvTime0045Podorojnik.setTextColor(tvTextColor[2]);
                    }
                    if (SecondDay >= (IntervalTime*4+1) & SecondDay <= (IntervalTime*5)){
                        // Фокус TextView время.
                        tvTime0100Podorojnik.requestFocus();
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0100Podorojnik.setBackgroundColor(tvBackgroundColor[4]);
                        tvTime0100Podorojnik.setTextColor(tvTextColor[4]);
                    } else {
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0100Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
                        tvTime0100Podorojnik.setTextColor(tvTextColor[2]);
                    }
                    if (SecondDay >= (IntervalTime*5+1) & SecondDay <= (IntervalTime*6)){
                        // Фокус TextView время.
                        tvTime0115Podorojnik.requestFocus();
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0115Podorojnik.setBackgroundColor(tvBackgroundColor[4]);
                        tvTime0115Podorojnik.setTextColor(tvTextColor[4]);
                    } else {
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0115Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
                        tvTime0115Podorojnik.setTextColor(tvTextColor[2]);
                    }
                    if (SecondDay >= (IntervalTime*6+1) & SecondDay <= (IntervalTime*7)){
                        // Фокус TextView время.
                        tvTime0130Podorojnik.requestFocus();
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0130Podorojnik.setBackgroundColor(tvBackgroundColor[4]);
                        tvTime0130Podorojnik.setTextColor(tvTextColor[4]);
                    } else {
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0130Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
                        tvTime0130Podorojnik.setTextColor(tvTextColor[2]);
                    }
                    if (SecondDay >= (IntervalTime*7+1) & SecondDay <= (IntervalTime*8)){
                        // Фокус TextView время.
                        tvTime0145Podorojnik.requestFocus();
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0145Podorojnik.setBackgroundColor(tvBackgroundColor[4]);
                        tvTime0145Podorojnik.setTextColor(tvTextColor[4]);
                    } else {
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0145Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
                        tvTime0145Podorojnik.setTextColor(tvTextColor[2]);
                    }
                    if (SecondDay >= (IntervalTime*8+1) & SecondDay <= (IntervalTime*9)){
                        // Фокус TextView время.
                        tvTime0200Podorojnik.requestFocus();
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0200Podorojnik.setBackgroundColor(tvBackgroundColor[4]);
                        tvTime0200Podorojnik.setTextColor(tvTextColor[4]);
                    } else {
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0200Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
                        tvTime0200Podorojnik.setTextColor(tvTextColor[2]);
                    }
                    if (SecondDay >= (IntervalTime*9+1) & SecondDay <= (IntervalTime*10)){
                        // Фокус TextView время.
                        tvTime0215Podorojnik.requestFocus();
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0215Podorojnik.setBackgroundColor(tvBackgroundColor[4]);
                        tvTime0215Podorojnik.setTextColor(tvTextColor[4]);
                    } else {
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0215Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
                        tvTime0215Podorojnik.setTextColor(tvTextColor[2]);
                    }
                    if (SecondDay >= (IntervalTime*10+1) & SecondDay <= (IntervalTime*11)){
                        // Фокус TextView время.
                        tvTime0230Podorojnik.requestFocus();
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0230Podorojnik.setBackgroundColor(tvBackgroundColor[4]);
                        tvTime0230Podorojnik.setTextColor(tvTextColor[4]);
                    } else {
                        // Установка цвета фона TextView для времени. Установка цвета текста TextView для времени.
                        tvTime0230Podorojnik.setBackgroundColor(tvBackgroundColor[2]);
                        tvTime0230Podorojnik.setTextColor(tvTextColor[2]);
                    }

                    // Преобразуем информацию в строковые данные
                    String strDate = simpleDateFormat.format(calendar.getTime());

                    // Всплывающее окно
                    Toast toastTimer = Toast.makeText(getApplicationContext() ,  strDate  + " " +  hourDaySystem + " " + minuteSystem + " " + SecondDay, Toast.LENGTH_LONG);
                    toastTimer.setGravity(Gravity.CENTER, 0, 0);
                    toastTimer.show();
                }
            });

        }
    }
    // Конец метода для описания того, что будет происходить при работе таймера (задача для таймера).
    // Начало метода сохранения.
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("bufferAddress", tvAddressSchedulePodorojnik.getText().toString());
        outState.putString("bufferBox", tvBoxPodorojnik.getText().toString());
        outState.putString("bufferDate", bnDateSchedulePodorojnik.getText().toString());
    }
    // Конец метода сохранения.
    // Начало метода восстановления.
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        tvAddressSchedulePodorojnik.setText(savedInstanceState.getString("bufferAddress"));
        tvBoxPodorojnik.setText(savedInstanceState.getString("bufferBox"));
        bnDateSchedulePodorojnik.setText(savedInstanceState.getString("bufferDate"));
    }
    // Конец метода восстановления.
}
