package com.example.abm.podorojnik;

import java.util.ArrayList;
import java.util.List;

class Wash {
    private int id;
    private String name, descr;
    private ArrayList<Box> boxes;

    public Wash(int id, String name, String descr) {
        this.id = id;
        this.name = name;
        this.descr = descr;
        boxes = new ArrayList<Box>();
    }

    public int getID() { return id; }
    public String getName() { return name; }
    public String getDesctiption() { return descr; }
    public void addBox(int id, String description) { boxes.add(new Box(id, description)); }
    public List<Box> getBoxes() { return (List<Box>)boxes.clone(); }
}
