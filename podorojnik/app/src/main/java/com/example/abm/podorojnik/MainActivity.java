package com.example.abm.podorojnik;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Устанавливаем заголовок.
        setTitle("ПОДОРОЖНИК");

        // Создаём макет.
        LinearLayout linearLayoutBeginPodorojnik = new LinearLayout(this);

        // Вертикальная ореинтация.
        linearLayoutBeginPodorojnik.setOrientation(LinearLayout.VERTICAL);

        // Установка цвета фона макета.
        linearLayoutBeginPodorojnik.setBackgroundColor(0xff000000);

        // Создаём TextView.
        TextView tvDescriptionPodorojnik = new TextView(this);

        // Текст на TextView.
        tvDescriptionPodorojnik.setText("Сервис онлайн записи на автомойки города");

        // Установка размера шрифта TextView.
        tvDescriptionPodorojnik.setTextSize(22);

        // Установка цвета текста TextView.
        tvDescriptionPodorojnik.setTextColor(0xffffffff);

        // Настраиваем вид TextView на макете.
        LinearLayout.LayoutParams layoutParamsDescriptionPodorojnik = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // Установка внешних отступов TextView на макете.
        layoutParamsDescriptionPodorojnik.setMargins(20, 20, 20, 20);

        // Установка внешних отступов на TextView.
        tvDescriptionPodorojnik.setLayoutParams(layoutParamsDescriptionPodorojnik);

        // Добавляем TextView на макет.
        linearLayoutBeginPodorojnik.addView(tvDescriptionPodorojnik);

        // Создаём кнопку.
        Button bnRecordSinkPodorojnik = new Button(this);
        Button bnPersonalAccountPodorojnik = new Button(this);
        Button bnSchedulePodorojnik = new Button(this);

        // Текст на кнопке.
         bnRecordSinkPodorojnik.setText("ЗАПИСАТЬСЯ НА МОЙКУ");
         bnPersonalAccountPodorojnik.setText("ЛИЧНЫЙ КАБИНЕТ");
         bnSchedulePodorojnik.setText("РАСПИСАНИЕ");

        // Установка цвета текста Button.
        bnRecordSinkPodorojnik.setTextColor(0xffffffff);
        bnPersonalAccountPodorojnik.setTextColor(0xffffffff);
        bnSchedulePodorojnik.setTextColor(0xffffffff);

         // Настраиваем вид кнопки на макете.
        LinearLayout.LayoutParams layoutParamsRecordSinkPodorojnik = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams layoutParamsPersonalAccountPodorojnik = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams layoutParamsSchedulePodorojnik = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // Установка внешних отступов кнопки на макете.
        layoutParamsRecordSinkPodorojnik.setMargins(20,20, 20, 20);
        layoutParamsPersonalAccountPodorojnik.setMargins(20, 20, 20, 20);
        layoutParamsSchedulePodorojnik.setMargins(20, 20, 20, 20);

        // Установка внешних отступов на кнопке.
        bnRecordSinkPodorojnik.setLayoutParams(layoutParamsRecordSinkPodorojnik);
        bnPersonalAccountPodorojnik.setLayoutParams(layoutParamsPersonalAccountPodorojnik);
        bnSchedulePodorojnik.setLayoutParams(layoutParamsSchedulePodorojnik);

         // Добавляем кнопку на макет.
        linearLayoutBeginPodorojnik.addView(bnRecordSinkPodorojnik);
        linearLayoutBeginPodorojnik.addView(bnPersonalAccountPodorojnik);
        linearLayoutBeginPodorojnik.addView(bnSchedulePodorojnik);

        // Обработка события нажатия кнопки.
        bnRecordSinkPodorojnik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // действия, совершаемые после нажатия на кнопку
                // Создаем объект Intent для вызова новой Activity
                // в первом параметре текущий класс. во втором - класс для перехода
                Intent intentRecordSinkPodorojnik = new Intent(MainActivity.this, Main2ActivityRecordSinkPodorojnik.class);

                // запуск activity
                startActivity(intentRecordSinkPodorojnik);

                // abm.
                Toast toastABM1 = Toast.makeText(getApplicationContext(), "ЗАПИСАТЬСЯ НА МОЙКУ", Toast.LENGTH_LONG);
                toastABM1.show();
            }
        });
        bnPersonalAccountPodorojnik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // действия, совершаемые после нажатия на кнопку
                // Создаем объект Intent для вызова новой Activity
                // в первом параметре текущий класс. во втором - класс для перехода
                Intent intentPersonalAccountPodorojnik = new Intent(MainActivity.this, Main3ActivityPersonalAccountPodorojnik.class);

                // запуск activity
                startActivity(intentPersonalAccountPodorojnik);

                // abm.
                Toast toastABM2 = Toast.makeText(getApplicationContext(), "ЛИЧНЫЙ КАБИНЕТ", Toast.LENGTH_LONG);
                toastABM2.show();
            }
        });
        bnSchedulePodorojnik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // действия, совершаемые после нажатия на кнопку
                // Создаем объект Intent для вызова новой Activity
                // в первом параметре текущий класс. во втором - класс для перехода
                Intent intentSchedulePodorojnik = new Intent(MainActivity.this, Main4ActivitySchedulePodorojnik.class);

                // запуск activity
                startActivity(intentSchedulePodorojnik);

                // abm.
                Toast toastABM3 = Toast.makeText(getApplicationContext(), "РАСПИСАНИЕ", Toast.LENGTH_LONG);
                toastABM3.show();
            }
        });

        // Отображаем макет.
        setContentView(linearLayoutBeginPodorojnik);
    }
}
