package com.example.abm.podorojnik;

/**
 * Created by ABM on 05.03.2018.
 */

public class Box {
    private int id;
    private String description;

    public Box(int id, String description) {
        this.id = id;
        this.description = new String(description);
    }

    public int getId() { return id; }
    public String getDescription() { return description; }
}
