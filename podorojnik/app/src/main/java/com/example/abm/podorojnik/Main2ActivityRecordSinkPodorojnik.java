package com.example.abm.podorojnik;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.Layout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Main2ActivityRecordSinkPodorojnik extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Устанавливаем заголовок.
        setTitle("ЗАПИСЬ НА МОЙКУ");

        // Создаём макет.
        LinearLayout linearLayoutRecordSinkPodorojnik = new LinearLayout(this);

        // Вертикальная ореинтация.
        linearLayoutRecordSinkPodorojnik.setOrientation(LinearLayout.VERTICAL);

        // Установка цвета фона макета.
        linearLayoutRecordSinkPodorojnik.setBackgroundColor(0xff000000);

        // Создаём TextView.
        TextView tvDescriptionRecordSinkPodorojnik = new TextView(this);

        // Текст на TextView.
        tvDescriptionRecordSinkPodorojnik.setText("Введите номер вашего мобильного телефона");

        // Установка размера шрифта TextView.
        tvDescriptionRecordSinkPodorojnik.setTextSize(22);

        // Установка цвета текста TextView.
        tvDescriptionRecordSinkPodorojnik.setTextColor(0xffffffff);

        // Настраиваем вид TextView на макете.
        LinearLayout.LayoutParams layoutParamsDescriptionRecordSinkPodorojnik = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // Установка внешних отступов TextView на макете.
        layoutParamsDescriptionRecordSinkPodorojnik.setMargins(20, 20, 20, 20);

        // Установка внешних отступов на TextView.
        tvDescriptionRecordSinkPodorojnik.setLayoutParams(layoutParamsDescriptionRecordSinkPodorojnik);

        // Добавляем TextView на макет.
        linearLayoutRecordSinkPodorojnik.addView(tvDescriptionRecordSinkPodorojnik);

        // Создаём EditText.
        final EditText etPhoneRecordSinkPodorojnik = new  EditText(this);

        // Установка цвета текста EditText.
        etPhoneRecordSinkPodorojnik.setTextColor(0xffffffff);

        // Установка цвета фона EditText.
        etPhoneRecordSinkPodorojnik.setBackgroundColor(0xff808080);

        // Настраиваем вид EditText на макете.
        LinearLayout.LayoutParams layoutParamsPhoneRecordSinkPodorojnik = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // Установка внешних отступов EditText на макете.
        layoutParamsPhoneRecordSinkPodorojnik.setMargins(20, 20, 20, 20);

        // Установка внешних отступов на EditText.
        etPhoneRecordSinkPodorojnik.setLayoutParams(layoutParamsPhoneRecordSinkPodorojnik);

        // Устанавливает текст в EditText.
        etPhoneRecordSinkPodorojnik.setHint("+7(");

        // Устанавливает тип клавиатуры в EditText.
        etPhoneRecordSinkPodorojnik.setInputType(InputType.TYPE_CLASS_PHONE);

        // Добавляем EditText на макет.
        linearLayoutRecordSinkPodorojnik.addView(etPhoneRecordSinkPodorojnik);

        // Создаём кнопку.
        Button bnContinueRecordSinkPodorojnik = new Button(this);

        // Текст на кнопке.
        bnContinueRecordSinkPodorojnik.setText("ПРОДОЛЖИТЬ");

        // Установка цвета текста Button.
        bnContinueRecordSinkPodorojnik.setTextColor(0xffffffff);

        // Настраиваем вид кнопки на макете.
        LinearLayout.LayoutParams layoutParamsContinueRecordSinkPodorojnik = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // Установка внешних отступов кнопки на макете.
        layoutParamsContinueRecordSinkPodorojnik.setMargins(20, 20, 20, 20);

        // Установка внешних отступов на кнопке.
        bnContinueRecordSinkPodorojnik.setLayoutParams(layoutParamsContinueRecordSinkPodorojnik);

        // Добавляем кнопку на макет.
        linearLayoutRecordSinkPodorojnik.addView(bnContinueRecordSinkPodorojnik);

        // Обработка события нажатия кнопки.
        bnContinueRecordSinkPodorojnik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // abm.
                Toast toast = Toast.makeText(getApplicationContext(), etPhoneRecordSinkPodorojnik.getText(), Toast.LENGTH_LONG);
                toast.show();
            }
        });

        // Отображаем макет.
        setContentView(linearLayoutRecordSinkPodorojnik);


    }
}
