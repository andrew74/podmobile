package com.example.abm.podorojnik;

import java.util.Set;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import java.io.InputStream;
import java.io.InputStreamReader;

class PodkomiHelper {
    static private JsonElement doRequest(String path) {
        String requestUrl = "http://podkomi.ru/mobile/" + path;
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setReadTimeout(10000);
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);

            urlConnection.connect();
            int responseCode = urlConnection.getResponseCode();
            if (responseCode != 200) {
                System.err.println("DoRequest: err code: %d\n");
                return null;
            }
            InputStreamReader in = new InputStreamReader((InputStream) urlConnection.getContent());
            JsonReader reader = new JsonReader(in);
            JsonParser parser = new JsonParser();
            return parser.parse(reader);
        } catch (Exception e) {
            System.err.printf("PodkomiHelper: Err doing %s\n", path);
            e.printStackTrace();
        }

        return null;
    }
    static public ArrayList<Wash> getWashes() {
        ArrayList<Wash> list = new ArrayList<Wash>();
        JsonArray arr = doRequest("get_addresses").getAsJsonArray();
        for (JsonElement elem : arr) {
            JsonArray addrArr = elem.getAsJsonArray();
            Wash wash = new Wash(addrArr.get(0).getAsInt(),
                    addrArr.get(1).getAsString(),
                    addrArr.get(2).getAsString());

            JsonObject boxesObj =  addrArr.get(3).getAsJsonObject();
            Set<String> keys = boxesObj.keySet();
            for (String key : keys) {
                int id = Integer.parseInt(key);
                String description = boxesObj.get(key).getAsString();
                wash.addBox(id, description);
            }
            list.add(wash);
        }

        return list;
    }
}