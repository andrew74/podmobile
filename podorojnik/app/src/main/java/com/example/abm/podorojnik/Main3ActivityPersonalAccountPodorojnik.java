package com.example.abm.podorojnik;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Main3ActivityPersonalAccountPodorojnik extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Устанавливаем заголовок.
        setTitle ("ВХОД В ЛИЧНЫЙ КАБИНЕТ");

        // Создаём макет.
        LinearLayout linearLayoutPersonalAccountPodorojnik = new LinearLayout(this);

        // Вертикальная ореинтация.
        linearLayoutPersonalAccountPodorojnik.setOrientation(LinearLayout.VERTICAL);

        // Установка цвета фона макета.
        linearLayoutPersonalAccountPodorojnik.setBackgroundColor(0xff000000);

        // Создаём TextView.
        TextView tvDescriptionPersonalAccountPodorojnik = new TextView(this);

        // Текст на TextView.
        tvDescriptionPersonalAccountPodorojnik.setText("Введите номер вашего мобильного телефона");

        // Установка размера шрифта TextView.
        tvDescriptionPersonalAccountPodorojnik.setTextSize(22);

        // Установка цвета текста TextView.
        tvDescriptionPersonalAccountPodorojnik.setTextColor(0xffffffff);

        // Настраиваем вид TextView на макете.
        LinearLayout.LayoutParams layoutParamsDescriptionPersonalAccountPodorojnik = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // Установка внешних отступов TextView на макете.
        layoutParamsDescriptionPersonalAccountPodorojnik.setMargins(20, 20 ,20,20);

        // Установка внешних отступов на TextView.
        tvDescriptionPersonalAccountPodorojnik.setLayoutParams(layoutParamsDescriptionPersonalAccountPodorojnik);

        // Добавляем TextView на макет.
        linearLayoutPersonalAccountPodorojnik.addView(tvDescriptionPersonalAccountPodorojnik);

        // Создаём EditText.
        final EditText etPhonePersonalAccountPodorojnik = new EditText(this);

        // Установка цвета текста EditText.
        etPhonePersonalAccountPodorojnik.setTextColor(0xffffffff);

        // Установка цвета фона EditText.
        etPhonePersonalAccountPodorojnik.setBackgroundColor(0xff808080);

        // Настраиваем вид EditText на макете.
        LinearLayout.LayoutParams layoutParamsPhonePersonalAccountPodorojnik = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // Установка внешних отступов EditText на макете.
        layoutParamsPhonePersonalAccountPodorojnik.setMargins(20, 20, 20, 20);

        // Установка внешних отступов на EditText.
        etPhonePersonalAccountPodorojnik.setLayoutParams(layoutParamsPhonePersonalAccountPodorojnik);

        // Устанавливает текст в EditText.
        etPhonePersonalAccountPodorojnik.setHint("+7(");

        // Устанавливает тип клавиатуры в EditText.
        etPhonePersonalAccountPodorojnik.setInputType(InputType.TYPE_CLASS_PHONE);

        // Добавляем EditText на макет.
        linearLayoutPersonalAccountPodorojnik.addView(etPhonePersonalAccountPodorojnik);

        // Создаём кнопку.
        Button bnContinuePersonalAccountPodorojnik = new Button(this);

        // Текст на кнопке.
        bnContinuePersonalAccountPodorojnik.setText("ПРОДОЛЖИТЬ");

        // Установка цвета текста Button.
        bnContinuePersonalAccountPodorojnik.setTextColor(0xffffffff);

        // Настраиваем вид кнопки на макете.
        LinearLayout.LayoutParams layoutParamsContinuePersonalAccountPodorojnik = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // Установка внешних отступов кнопки на макете.
        layoutParamsContinuePersonalAccountPodorojnik.setMargins(20,20,20,20);

        // Установка внешних отступов на кнопке.
        bnContinuePersonalAccountPodorojnik.setLayoutParams(layoutParamsContinuePersonalAccountPodorojnik);

        // Добавляем кнопку на макет.
        linearLayoutPersonalAccountPodorojnik.addView(bnContinuePersonalAccountPodorojnik);

        // Обработка события нажатия кнопки.
        bnContinuePersonalAccountPodorojnik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //abm.
                Toast toast = Toast.makeText(getApplicationContext(), etPhonePersonalAccountPodorojnik.getText(), Toast.LENGTH_LONG);
                toast.show();
            }
        });

        // Отображаем макет.
        setContentView(linearLayoutPersonalAccountPodorojnik);

    }
}
